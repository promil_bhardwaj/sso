import * as dataService from 'qwest';
import userService from "../services/userService";

declare let Fingerprint2;
class SSO {
    private fingerPrint: any;
    private location: any;

    constructor(){

    }

    init() {
        this.registerEvents();
        userService.getMetaData(false).then(function (user, data) {
            if (user && user.resetPassword === true) {
                //page("/resetPassword");
            } else {
                // _this.sessionService.save();
                // _this.checkAccounts();
            }
        });
        this.fingerPrint = this.getFingerPrint();
        this.location = this.getLocation();

    }

    registerEvents() {
        document.getElementById("loginBtn").addEventListener('click', (e) => {
            e.preventDefault();
            this.formActionCalled(e);
        });
        document.getElementById("username").addEventListener('keyup', (e) => {
            this.clearErrors();
        });
        document.getElementById('password').addEventListener('keyup', (e) => {
            this.clearErrors();
        });
    }

    getFingerPrint() {
        return new Promise((resolve, reject) => {
            new Fingerprint2().get(function (fingerPrint, components) {
                resolve(fingerPrint);
            });

        })

    }

    getLocation() {
        return new Promise((resolve, reject) => {
            if (window.navigator.geolocation)
                window.navigator.geolocation.watchPosition(function (position) {
                        resolve(position);
                    },
                    function (error) {
                        resolve(error);
                    });
        });

    }

    formActionCalled(e) {
        let isValid = this.isFormValid();
        if (isValid) {
            this.submitForm(e);
        }
    }

    isFormValid() {
        let emailInError = false,
            passInError = false,
            email = (<HTMLInputElement>document.getElementById("username")).value,
            password = (<HTMLInputElement>document.getElementById("password")).value;

        let emailError = document.getElementById("emailError"),
            passError = document.getElementById("passError");

        let emailExp = /\S+@\S+\.\S+/,
            numberExp = /[0-9]/;

        if (!new RegExp(emailExp).test(email) && !(new RegExp(numberExp).test(password) && password.length === 10)) {
            emailError.classList.add('show');
            emailInError = true;
        }
        if (password.trim().length === 0) {
            passError.classList.add('show');
            passInError = true;
        }
        return emailInError && passInError;
    }

    clearErrors() {
        let emailError = document.getElementById("emailError"),
            formError = document.getElementById("formError"),
            passError = document.getElementById("passError");

        emailError.classList.remove('show');
        passError.classList.remove('show');
        formError.classList.remove('show');
    }

    submitForm(e) {
        let self = this;

        Promise.all([this.fingerPrint, this.location]).then(function (values) {
            let fingerPrint = values[0];
            let locationInfo = values[1];

            (<HTMLInputElement>document.getElementById("deviceid")).value = fingerPrint;
            if (locationInfo && locationInfo.coords) {
                (<HTMLInputElement>document.getElementById("location_latitude")).value= locationInfo.coords.latitude;
                (<HTMLInputElement>document.getElementById("#location_longitude")).value = locationInfo.coords.longitude;
            }
            // self.loginWithInfo(e);
        });
    }
}
let instance  = new SSO();
export default instance
//let is the key here . So that it always returns the same instance to make it singeleton