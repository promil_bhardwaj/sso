let CONSTANTS = {
    api: {
        "LOGIN": "",
        "GET_USER_ACCOUNTS": "/data/user/accounts",
        "GET_AUTH_TOKEN": "/data/auth/ticket/grant",
        "FORGOT_PASSWORD": "/data/user/forgotPassword",
        "RESET_PASSWORD": "/data/user/otp/reset",
        "GET_USER_INFO": "/data/user/info"
    }
};

export default CONSTANTS;


