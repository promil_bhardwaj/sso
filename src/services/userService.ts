import CONSTANTS from './constants';
import * as dataService from 'qwest';
import instacnce from "../components/sso";


// let userService = (function () {
//     let instance;
//     let data;
//
//     function createInstance() {
//         return {
//             getMetadata: (force)=> {
//                 if (data.user === undefined || force === true) {
//                     return dataService.post(CONSTANTS.api.GET_USER_INFO, {}).then(function (xhr, resp) {
//                         if (resp.successful === true) {
//                             data.user = resp.user;
//                         }
//                         return data.user;
//                     });
//                 } else {
//                     return Promise.resolve(data.user);
//                 }
//             },
//
//             getAccounts: (force) => {
//                 if (data.accounts === undefined || force === true) {
//                     return dataService.post(CONSTANTS.api.GET_USER_ACCOUNTS, {}).then((xhr, resp) => {
//                         if (resp.successful === true) {
//                             data.accounts = resp.accounts;
//                         }
//                         return data.accounts;
//                     })
//                 } else {
//                     return Promise.resolve(data.accounts);
//                 }
//             },
//
//             set: (key, value)=> {
//                 if (key !== undefined && value !== undefined) {
//                     data[key] = value;
//                 }
//             },
//
//             get: (key)=> {
//                 if (key !== undefined) {
//                     return data[key];
//                 }
//             },
//
//             reset : (key)=>{
//                 if (key && key !== "ALL" && data[key] !== undefined) {
//                     delete data[key];
//                 } else {
//                     data = {
//                         user: undefined,
//                         accounts: undefined
//                     };
//                 }
//             }
//         }
//     }
//
//
//     return {
//         getInstance: function () {
//             if (!instance) {
//                 instance = createInstance();
//             }
//             return instance
//         }
//     }
//
// })();
//
//
// let instance = userService.getInstance();
// export default instance;


class UserService {
    data = {
        user: undefined,
        accounts: undefined
    };
    constructor(){

    }
    getMetaData(force) {
        if (this.data.user === undefined || force === true) {
            return dataService.post(CONSTANTS.api.GET_USER_INFO, {}).then(function (xhr, resp) {
                if (resp.successful === true) {
                    this.data.user = resp.user;
                }
                return this.data.user;
            });
        } else {
            return Promise.resolve(this.data.user);
        }
    }

    getAccounts(force) {
        if (this.data.accounts === undefined || force === true) {
            return dataService.post(CONSTANTS.api.GET_USER_ACCOUNTS, {}).then((xhr, resp) => {
                if (resp.successful === true) {
                    this.data.accounts = resp.accounts;
                }
                return this.data.accounts;
            })
        } else {
            return Promise.resolve(this.data.accounts);
        }
    }

    set(key, value) {
        if (key !== undefined && value !== undefined) {
            this.data[key] = value;
        }
    }

    get(key) {
        if (key !== undefined) {
            return this.data[key];
        }
    }

    reset(key) {
        if (key && key !== "ALL" && this.data[key] !== undefined) {
            delete this.data[key];
        } else {
            this.data = {
                user: undefined,
                accounts: undefined
            };
        }
    }

}
let instance = new UserService();
export default instance;