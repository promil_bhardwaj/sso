module.exports = function (env) {
    env = env || {};

    const path = require('path');
    const webpack = require('webpack');
    const HtmlWebpackPlugin = require('html-webpack-plugin');
    const ExtractTextPlugin = require("extract-text-webpack-plugin");
    const BabiliPlugin = require("babili-webpack-plugin");


    let isDev = env.dev || false;
    let isProd = env.prod || false;
    const config = {
        devtool: 'source-map',
        entry: {
            scripts: [
                "script-loader!./node_modules/fingerprintjs2/dist/fingerprint2.min.js"
            ],
            styles: './src/main.css',
            main: './src/main.ts'
        },
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: isProd ? '[name].[hash].bundle.js' : '[name].bundle.js',
            sourceMapFilename: isProd ? '[name].[hash].map' : '[name].map',
            chunkFilename: isProd ? '[id].[hash].chunk.js' : '[id].chunk.js',
            publicPath: '/'
        },
        resolve: {
            extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
        },
        devServer: {
            publicPath: "/",
            compress: false,
            port: 9000,
            inline: true
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    loaders: [
                        'awesome-typescript-loader'
                    ],
                    exclude: [/\.(spec|e2e|d)\.ts$/]
                },
                {
                    test: /\.(js|jsx)$/,
                    exclude: /(node_modules|bower_components)/,
                    use: 'babel-loader'
                }, {
                    test: /\.css$/,
                    use: ExtractTextPlugin.extract({
                        use: ["css-loader"]
                    })
                },
                {
                    test: /\.scss$/,
                    use: ExtractTextPlugin.extract({
                        use: ['css-loader', 'sass-loader']
                    })
                }
            ]
        },
        plugins: [
            new ExtractTextPlugin("main.css"),
            new HtmlWebpackPlugin({template: './src/index.html'}),
            new webpack.optimize.CommonsChunkPlugin({
                name: ['styles','main','globals']
            }),

        ]
    };

    if (isProd) {
        config.plugins.push(new BabiliPlugin({
            removeDebugger: false,
            removeConsole: false
        }, {}))
    }

    return config;

};